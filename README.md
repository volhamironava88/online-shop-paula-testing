# Online shop Paula testing
Объектом тестирования является сайт https://www.paula.by/ , содержащий  основные  функции  интернет-магазина.\
Используя  сайт,  пользователь  может:  
- просматривать каталог товаров, 
- искать товары, 
- смотреть описание товаров, 
- добавить в корзину/избранное, 
- оформлять заявку, 
- делиться описанием товаров с друзьями и др.
 
 Тестовая документация:\
 Тест план https://docs.google.com/document/d/1tKGckUzBZOjMsZuExYVMXm_8oXErUigkPUaFwDOxucU/edit?usp=sharing \
 Тест-кейсы https://docs.google.com/document/d/1hoHaraJ4cUJnpBpaUnVaWkJfEASF77C72Wu0PJxXND0/edit?usp=sharing
 
